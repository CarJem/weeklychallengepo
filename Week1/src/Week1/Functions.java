package Week1;

import java.math.BigInteger;

public class Functions {
	
	/**
	 * Tries to get the LCM of the Array
	 * 
	 * @param number_array
	 * An array of integers
	 * @return
	 * The LCM of all the items in the array (if it fails, it returns -1)
	 */
	public static long tryLcmOfArray(int number_array[]) {
		try {
			return lcmOfArray(number_array);
		}
		catch (Exception ex) {
			System.out.println(ex.getMessage());
			return -1;
		}
	}

	/**
	 * Gets the LCM of the Array
	 * 
	 * @param number_array
	 * An array of integers
	 * @return
	 * The LCM of all the items in the array
	 * @throws Exception
	 * Likely thrown if there is a negative number
	 */
	public static long lcmOfArray(int number_array[]) throws Exception {
		//the minimum possible lcm in the cases provided is 1
		long lcm = 1;
		int divisor = 2;
		
		while (true) {
			
			int counter = 0;
			boolean isDivisible = false;
			
			//Find the next possible divisible number
			for (int i = 0; i < number_array.length; i++) {
				//lcm of any array with of 0 is ALWAYS 0, return 0;
				if (number_array[i] == 0) return 0;
				//Does not support negative numbers
				else if (number_array[i] < 0) throw new Exception("Negative Numbers not Allowed!");
				//if the element is 1, skip to the next
				if (number_array[i] == 1) counter++;
				
				if (number_array[i] % divisor == 0) {
					isDivisible = true;
					number_array[i] = number_array[i] / divisor;	
				}
			}
			
			//if it is divisible by the divisor, this is our new lcm, otherwise, keep going and increase the divisor by one
			if (isDivisible) lcm = lcm * divisor;
			else divisor++;
			
			//once we have checked every single item in the array, terminate the while loop and return the lcm
			if (counter == number_array.length) return lcm;
		}
	}
	
	/**
	 * This function does the following:
	 * 
	 * - Split the number into groups of two digit numbers. (If the number has an odd number of digits, return -1)
	 * - For each group of two digit numbers, concatenate the last digit to a new string the same number of times as the value of the first digit.
	 * - Return the result as an integer.
	 * @param val
	 * the number to perform work on
	 * @return
	 * the result of the function's logic
	 */
	public static BigInteger lookAndSay(int i) {
		return lookAndSay(BigInteger.valueOf(i));
	}
	
	/**
	 * This function does the following:
	 * 
	 * - Split the number into groups of two digit numbers. (If the number has an odd number of digits, return -1)
	 * - For each group of two digit numbers, concatenate the last digit to a new string the same number of times as the value of the first digit.
	 * - Return the result as an integer.
	 * @param val
	 * the number to perform work on
	 * @return
	 * the result of the function's logic
	 */
	public static BigInteger lookAndSay(long i) {
		return lookAndSay(BigInteger.valueOf(i));
	}

	/**
	 * This function does the following:
	 * 
	 * - Split the number into groups of two digit numbers. (If the number has an odd number of digits, return -1)
	 * - For each group of two digit numbers, concatenate the last digit to a new string the same number of times as the value of the first digit.
	 * - Return the result as an integer.
	 * @param val
	 * the number to perform work on
	 * @return
	 * the result of the function's logic
	 */
	public static BigInteger lookAndSay(BigInteger val) {
		
		//Determine the number of digits
		int digitCount = val.toString().length();
		
		//If the number of digits is odd, return 
		if (digitCount % 2 != 0) return BigInteger.valueOf(-1);
		
		String result = "";
		
		//Cycle through the array
		for (int i = 0; i < digitCount && i + 1 < digitCount; i = i + 2) {
			//get the number of times to repeat the second digit
			int repeatCount = Character.getNumericValue(val.toString().charAt(i));
			//get the value of the second digit
			var num = val.toString().charAt(i+1);
			//add the 'num' to the result string 'repeatCount' times
			for (int r = 0; r < repeatCount; r++) {
				result += num;
			}
		}
		
		//return the result as a BigInteger
		return new BigInteger(result);
		
	}
}

package Week1;

public class Main {

	public static void main(String[] args) {	
		//PART 1: LCM Function
		System.out.println("PART 1: LCM Function");
		System.out.println(Functions.tryLcmOfArray(new int[] {5, 4, 6, 46, 54, 12, 13, 17}));
		System.out.println(Functions.tryLcmOfArray(new int[] {46, 54, 466, 544}));
		System.out.println(Functions.tryLcmOfArray(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
		System.out.println(Functions.tryLcmOfArray(new int[] {13, 6, 17, 18, 19, 20, 37}));
		
		//PART 2: Look and Say
		System.out.println("PART 2: Look and Say");
		System.out.println(Functions.lookAndSay(3132));
		System.out.println(Functions.lookAndSay(1213200012171979L));
		System.out.println(Functions.lookAndSay(54544666));
		System.out.println(Functions.lookAndSay(95));
		System.out.println(Functions.lookAndSay(1213141516171819L));
		System.out.println(Functions.lookAndSay(120520));
		System.out.println(Functions.lookAndSay(231));
	}

}
